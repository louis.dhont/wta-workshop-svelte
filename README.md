# Svelte Workshop demo

- Odisee Electronics/ICT 2021-2022

In this workshop you will learn to create a todo application with the Svelte framework.

The backend API is already present and ready to be used.
The project only needs some extra code in order to function properly.

