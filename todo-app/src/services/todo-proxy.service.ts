import {CrudDatabase} from './crud-database.base';

export class TodoProxyService extends CrudDatabase<any> {

    async getAllTodos(): Promise<any> {
        const url = 'http://localhost:5050/todos/';
        const headers = {
            'Content-Type': 'application/json',
        };
        return await this.getRequest(url, headers);
    }

    async getTodoById(todoId: string): Promise<any> {
        const url = 'http://localhost:5050/todos/' + todoId;
        const headers = {
            'Content-Type': 'application/json',
        };
        return await this.getRequest(url, headers);
    }

    async patchTodoById(todoId: string, data): Promise<any> {
        const url = 'http://localhost:5050/todos/' + todoId;
        const headers = {
            'Content-Type': 'application/json',
        };
        return await this.patchRequest(url, data, headers);
    }

    async postNewTodo(data): Promise<any> {
        const url = 'http://localhost:5050/todos';
        const headers = {
            'Content-Type': 'application/json',
        };
        return await this.postRequest(url, data, headers);
    }

    async deleteTodo(todoId): Promise<any> {
        const url = 'http://localhost:5050/todos/' + todoId;
        return await this.deleteRequest(url);
    }
}
