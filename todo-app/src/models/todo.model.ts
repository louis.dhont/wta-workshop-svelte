export interface Todo {
    uuid: string;
    title: string;
    message: string;
    created_at: string;
}
