import type {Todo} from './todo.model';

export interface Todos {
    data: Todo[];
    totalCount: number;
}
