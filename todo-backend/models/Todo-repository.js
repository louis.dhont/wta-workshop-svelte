const mongoose = require('mongoose');

const todoSchema = new mongoose.Schema({
    uuid: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    created_at: {
        type: Number,
        default: Date.now
    }
}, {
    toJSON: {virtuals: true}
});

module.exports = mongoose.model('todos', todoSchema);
