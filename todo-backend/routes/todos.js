const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const {response} = require("express");
const Todos = mongoose.model('todos');

router.get('/', async (req, res) => {
    const todos = await Todos.find();
    res.json({
        data: todos,
        totalCount: await Todos.countDocuments()
    });
});

router.get('/:todoId', async (req, res) => {
    const todo = await Todos.findOne({uuid: req.params.todoId});
    res.json(todo);
});


router.post('/', async (req, res, next) => {
    try {
        const todo = await (new Todos({
            uuid: generateUUID(),
            title: req.body.title,
            message: req.body.message
        }).save());
        res.json({todo});
    } catch (err) {
        if (err.code === 11000) {
            next({
                status: 409,
                message: err.message
            });
        }
        next();
    }
});

router.patch('/:todoId', async (req, res, next) => {
    try {
        const todo = await Todos.findOne({uuid: req.params.todoId});
        if (todo.title) {
            await Todos.findOneAndUpdate({'uuid': req.params.todoId}, req.body, {upsert: true}, function (err, doc) {
                if (err) {
                    res.json({
                        status: 409,
                        message: err.message
                    });
                } else {
                    res.json(doc);
                }
            });
        }
    } catch (err) {
        if (err.code === 11000) {
            next({
                status: 409,
                message: err.message
            });
        }
        next();
    }
});

router.delete('/:todoId', async (req, res, next) => {
    const todo = await Todos.find({uuid: req.params.todoId}).remove();
    res.json({deletedCount: todo.deletedCount});
});

function generateUUID() {
    let dt = new Date().getTime();
    return 'xxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, (c) => {
        const r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return ((c === 'x') ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

module.exports = router;
